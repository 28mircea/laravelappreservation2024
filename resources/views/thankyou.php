<x-guest-layout>
    <div class="container w-full px-5 py-6 mx-auto">
        <h1>Thank you!</h1>
        <p>You reservation is ready!</p>
        <p><a href="/" class="block mt-4">Go to First Page!</a></p>
    </div>   
</x-guest-layout>

