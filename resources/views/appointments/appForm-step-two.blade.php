<x-guest-layout>                    
<div class="container w-full px-5 py-6 mx-auto">
    <br><br><br><br> <br><br><br><br>
    <div class="flex items-center min-h-screen bg-gray-50">        
        <div class="flex-1 h-full max-w-5xl mx-auto bg-white rounded-lg shadow-xl">
            <div class="flex flex-col md:flex-row">
                <div class="h-34 md:h-auto ld:w-1/1">
                    <img class="object-cover w-full h-full"
                        src="{{asset('frontend/images/reservation-image.jpg')}}" alt="img" />
                </div>
                <div class="flex items-center justify-center p-6 sm:p-12 md:w-1/1">
                    <div class="w-full">
                        <h3 class="mb-4 text-xl font-bold text-blue-600">Make Appointment</h3>
                        <div class="w-full bg-gray-200 rounded-full">
                            <div class="w-100 p-1 text-xs font-medium leading-none text-center text-blue-100 bg-blue-600 rounded-full">
                                Step 2
                            </div>
                        </div>
                        <form method="POST" action="{{route('appointments.storeAppForm.step.two')}}">
                            @csrf
                            <div class="sm:col-span-6">
                                <label for="doctor_id" class="block text-sm font-medium text-gray-700">Select Doctor</label>
                                <div class="sm:col-span-6">
                                    <select id="doctor_id" name="doctor_id" class="form-select form-select-sm">
                                        <option value="">(select doctor)</option>
                                        @foreach($doctors as $doctor)
                                            <option value="{{ $doctor->id }}">Dr. {{ $doctor->full_name }}({{$doctor->specialization->specialization_name}})</option> 
                                        @endforeach
                                    </select>
                                </div>
                                @error('doctor_id')
                                    <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                            </div>      
                            <div class="mt-6 p-4 flex justify-end">
                                <button type="submit"
                                    class="px-4 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">Make Appointment</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</x-guest-layout>    