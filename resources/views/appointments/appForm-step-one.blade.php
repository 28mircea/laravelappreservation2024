<x-guest-layout>
    @push('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/themes/dark.css">
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js"></script>                 
<div class="container w-full px-5 py-6 mx-auto">
    <br><br><br><br> <br><br><br><br>
    <div class="flex items-center min-h-screen bg-gray-50">        
        <div class="flex-1 h-full max-w-5xl mx-auto bg-white rounded-lg shadow-xl">
            <div class="flex flex-col md:flex-row">
                <div class="h-34 md:h-auto ld:w-1/1">
                    <img class="object-cover w-full h-full"
                        src="{{asset('frontend/images/reservation-image.jpg')}}" alt="img" />
                </div>
                <div class="flex items-center justify-center p-6 sm:p-12 md:w-1/1">
                    <div class="w-full">
                        <h3 class="mb-4 text-xl font-bold text-blue-600">Make Appointment</h3>
                        <div class="w-full bg-gray-200 rounded-full">
                            <div class="w-40 p-1 text-xs font-medium leading-none text-center text-blue-100 bg-blue-600 rounded-full">
                                Step1
                            </div>
                        </div>
                        <form method="POST" action="{{route('appointments.storeAppForm.step.one')}}">
                            @csrf
                            <div class="sm:col-span-6">
                                <label for="full_name" class="block text-sm font-medium text-gray-700"> Full Name
                                </label>
                                <div class="mt-1">
                                    <input type="text" id="first_name" name="full_name" placeholder="(Enter your name)"                                       
                                        class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                </div>
                                @error('full_name')
                                    <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                            </div>                            
                            <div class="sm:col-span-6">
                                <label for="email" class="block text-sm font-medium text-gray-700"> Email </label>
                                <div class="mt-1">
                                    <input type="email" id="email" name="email" placeholder="(Enter your email)"
                                        class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                </div>
                                @error('email')
                                    <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="sm:col-span-6">
                                <label for="tel_number" class="block text-sm font-medium text-gray-700"> Phone
                                    number
                                </label>
                                <div class="mt-1">
                                    <input type="text" id="tel_number" name="tel_number" placeholder="(Enter your phone number)"                                        
                                        class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                </div>
                                @error('tel_number')
                                    <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                            </div>                               
                            <div class="sm:col-span-6">
                                <label for="app_date" class="block text-sm font-medium text-gray-700"> Select Appointment
                                    Date
                                </label>
                                <div class="mt-1">
                                    <input type="date" id="app_date" name="app_date" placeholder="(Select appointment date)"
                                        class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                                </div>                                
                                @error('app_date')
                                    <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="sm:col-span-6">
                                <label for="app_time" class="block text-sm font-medium text-gray-700"> Select Appointment Time
                                </label>
                                <div class="sm:col-span-6">
                                    <select id="app_time" name="app_time" class="form-select form-select-sm">
                                            <option value="">(select appointment time)</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:30">10:30</option>
                                            <option value="12:00">12:00</option>
                                            <option value="15:30">15:30</option>
                                            <option value="17:00">17:00</option>
                                            <option value="18:30">18:30</option>
                                            <option value="20:00">20:00</option>
                                    </select>
                                </div>
                                @error('app_time')
                                    <div class="text-sm text-red-400">{{ $message }}</div>
                                @enderror
                            </div>                            
                            <div class="mt-6 p-4 flex justify-end">
                                <button type="submit"
                                    class="px-4 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">Next</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">        
    $("#app_date").flatpickr({
        minDate: "today",
        enableTime: true,
        dateFormat: "Y-m-d",
        "disable": [
            function(date) {
            return (date.getDay() === 0 || date.getDay() === 6);  // disable weekends
            }
        ],
        "locale": {
            "firstDayOfWeek": 1 // set start day of week to Monday
        }
    });
</script>    
</x-guest-layout>    