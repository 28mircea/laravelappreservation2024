<x-guest-layout>    
    <div class="md:flex md:flex-wrap mt-24 text-center md:-mx-4">        
        @foreach($specializations as $specialization)
            <div class="md:w-1/2 md:px-4 lg:w-2/2">
                <div class="bg-white rounded-lg border border-gray-300 p-8">
                    <img src="{{asset('frontend/images/clinic-plus-logo.png')}}" alt="" class="h-20 mx-auto">
                    <h4 class="text-xl font-bold mt-4">{{ $specialization->specialization_name }}</h4>                
                    <a href="{{ route('front.specializations.show', $specialization->id) }}" class="block mt-4">Show Doctors</a>
                </div>
            </div>
        @endforeach
    </div>        
</x-guest-layout>

