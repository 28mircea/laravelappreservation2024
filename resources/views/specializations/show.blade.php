<x-guest-layout>
    <div class="container w-full px-5 py-6 mx-auto">
        <br><br><br><br><br><br><br><br>
        <div class="grid lg:grid-cols-4 gap-y-6">
            @foreach ($specialization->doctors as $doctor)
                <div class="bg-white rounded-lg border border-gray-300 p-8">
                    <img class="h-48 mx-auto rounded" src="{{asset('storage/doctors/'.$doctor->image)}}" alt="Image" loading="lazy"/>
                    <div class="px-6 py-4 text-center">
                        <h4 class="mb-3 text-xl font-semibold tracking-tight text-green-600 uppercase">
                            Dr.{{ $doctor->full_name }}</h4>
                        <p class="leading-normal text-gray-700 text-center">
                            {{ $doctor->professional_statement }}
                        </p>
                        <p>
                            <span class="text-l text-green-600">Appointment price: ${{ $doctor->app_price }}</span>
                        </p>    
                    </div>
                    <div class="flex items-center justify-between p-2">
                        <a href="{{route('appointments.appForm')}}" class="px-2 py-2 bg-teal-500 text-white rounded inline-block mt-8 font-semibold mx-auto">Make
                            Appointment</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</x-guest-layout>    