@extends('admin.admin_dashboard')
@section('admin')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/themes/dark.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js"></script>


<div class="container">
    <div class="flex justify-end m-2 p-2">
        <h3 class="text-dark">Edit Appointment </h3>
        <a href="{{ route('appointments.index') }}" class="btn btn-primary">Appointments Index</a>
    </div>
    <div class="m-2 p-2">
        <div class="col-lg-6 margin-tb">
            <form method="POST" action="{{ route('appointments.update', $appointment->id) }}">
                @csrf
                @method('PUT')
                <div class="sm:col-span-6">
                    <label for="name" class="block text-sm font-medium text-gray-700"> Full Name </label>
                    <div class="mt-1">
                        <input type="text" id="full_name" name="full_name" value="{{ $appointment->full_name }}" class="form-control" />
                    </div>
                    @error('full_name')
                    <div class="text-sm text-red-400">{{ $message }}</div>
                    @enderror
                </div>
                <div class="sm:col-span-6">
                    <label for="email" class="block text-sm font-medium text-gray-700"> Email </label>
                    <div class="mt-1">
                        <input type="email" id="email" name="email" value="{{ $appointment->email }}" class="form-control" />
                    </div>
                    @error('email')
                    <div class="text-sm text-red-400">{{ $message }}</div>
                    @enderror
                </div>
                <div class="sm:col-span-6">
                    <label for="tel_number" class="block text-sm font-medium text-gray-700"> Phone number
                    </label>
                    <div class="mt-1">
                        <input type="text" id="tel_number" name="tel_number" value="{{ $appointment->tel_number }}" class="form-control" />
                    </div>
                    @error('tel_number')
                    <div class="text-sm text-red-400">{{ $message }}</div>
                    @enderror
                </div>
                <div class="sm:col-span-6">
                    <label for="app_date" class="block text-sm font-medium text-gray-700"> Appointment Date
                    </label>
                    <div class="mt-1">
                        <input type="date" id="app_date" name="app_date" value="{{ $appointment->app_date }}" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" class="form-control" />
                    </div>
                    @error('app_date')
                    <div class="text-sm text-red-400">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mt-1">
                    <label for="time" class="block text-sm font-medium text-gray-700"> Appointment Time
                    </label>
                    <select id="app_time" name="app_time" class="form-select form-select-sm">
                        <option value="{{$appointment->app_tine }}" @selected($appointment->app_time )>{{ $appointment->app_time->format('H:i')}}</option>
                        <option value="09:00">09:00</option>
                        <option value="10:30">10:30</option>
                        <option value="12:00">12:00</option>
                        <option value="15:30">15:30</option>
                        <option value="17:00">17:00</option>
                        <option value="18:30">18:30</option>
                        <option value="20:00">20:00</option>
                    </select>
                    @error('app_time')
                    <div class="text-sm text-red-400">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mt-1">
                    <label for="doctor_id" class="block text-sm font-medium text-gray-700">Select Doctor</label>
                    <div class="form-control">
                        <select id="doctor_id" name="doctor_id" class="form-select form-select-sm">
                            @foreach($doctors as $doctor)
                            <option value="{{ $doctor->id }}">Dr.{{ $doctor->full_name }}({{$doctor->specialization->specialization_name}})</option>
                            @endforeach
                        </select>
                    </div>
                    @error('doctor_id')
                    <div class="text-sm text-red-400">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mt-6 p-4">
                    <button type="submit" class="btn btn-info">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#app_date").flatpickr({
        minDate: "today",
        enableTime: true,
        dateFormat: "Y-m-d",
        "disable": [
            function(date) {
                return (date.getDay() === 0 || date.getDay() === 6); // disable weekends
            }
        ],
        "locale": {
            "firstDayOfWeek": 1 // set start day of week to Monday
        }
    });
</script>
@endsection