@extends('admin.admin_dashboard')
@section('admin')

<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.5.0/font/bootstrap-icons.min.css' />


<div class="container pt-3">
    <br>
    <div class="flex justify-end m-2 p-2">
        <a href="{{ route('appointments.create') }}" class="btn btn-primary">New Appointment</a>
    </div>
    <div class="relative overflow-x-auto">
        <table class="table table-bordered mt-3">
            <thead class="bg-dark text-white">
                <tr>
                    <th scope="col">
                        Full Name
                    </th>
                    <th scope="col">
                        Email
                    </th>
                    <th scope="col">
                        Phone number
                    </th>
                    <th scope="col">
                        Appointment Date
                    </th>
                    <th scope="col">
                        Appointment Hour
                    </th>
                    <th scope="col">
                        Doctor
                    </th>
                    <th scope="col">
                        <span class="sr-only">Actions</span>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($appointments as $appointment)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <td class="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap text-black">
                        {{ $appointment->full_name }}
                    </td>
                    <td class="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap text-black">
                        {{ $appointment->email }}
                    </td>
                    <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap text-black">
                        {{ $appointment->tel_number }}
                    </td>
                    <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap text-black">
                        {{ $appointment->app_date }}
                    </td>
                    <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap text-black">
                        {{ $appointment->app_time->format('H:i') }}
                    </td>
                    <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap text-black">
                        Dr. {{ $appointment->doctor->full_name }}
                    </td>
                    <td class="py-4 px-6 text-sm font-medium text-right whitespace-nowrap">
                        <div class="flex space-x-2">
                            <a href="{{ route('appointments.edit', $appointment->id) }}" class="btn btn-info">Edit</a>
                            <form class="px-4 py-2 bg-red-500 hover:bg-red-700 rounded-lg text-white" method="POST" action="{{ route('appointments.delete', $appointment->id) }}" onsubmit="return confirm('Are you sure?');">
                                @csrf
                                @method('DELETE')
                                <button type="submit"  class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @endsection