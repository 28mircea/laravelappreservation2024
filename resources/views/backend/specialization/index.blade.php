@extends('admin.admin_dashboard')
@section('admin')

<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

<div class="container pt-3">
    <div id="success_div" class="alert alert-success d-none">
        <span id="success_msg"></span>
    </div>
    <button type="button" class="btn btn-primary" id="add_specialization">Add Specialization</button>    
    <table class="table table-bordered mt-3">
        <thead class="bg-dark text-white">
            <th>Id</th>
            <th>Specialization Name</th>
            <th>Specialization Slug</th>
            <th>Action</th>
        </thead>
        <tbody id="list_specialization">
            @foreach($specializations as $item)
            <tr id="row_item_{{ $item->id}}">
                <td width="20">{{ $item->id}}</td>
                <td>{{ $item->specialization_name}}</td>
                <td>{{ $item->specialization_slug}}</td>
                <td width="150">
                    <button type="button" id="edit_item" data-id="{{ $item->id }}" class="btn btn-sm btn-info ml-1">Edit</button>

                    <button type="button" id="delete_item" data-id="{{ $item->id }}" class="btn btn-sm btn-danger ml-1">Delete</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<!-- The Modal -->
<div class="modal" id="modal_specialization">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="form_specialization">
                <div class="alert alert-danger print-error-msg" style="display:none">
                    <ul></ul>
                </div>      
            
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title"></h4>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <input type="text" name="specialization_name" id="specialization_name" class="form-control" placeholder="Enter specialization ...">
                    @error('specialization_name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Submit</button>                    
                </div>
            </form>

        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    //Add specialization
    $("#add_specialization").on('click', function() {
        $("#form_specialization").trigger('reset');
        $("#modal_title").html('Add specialization');
        $("#modal_specialization").modal('show');
        $("#id").val("");
    });

    // Edit Specialization
    $("body").on('click', '#edit_item', function() {
        var id = $(this).data('id');
        $.get('specializations/' + id + '/edit', function(res) {
            $("#modal_title").html('Edit Specialization');
            $("#id").val(res.id);
            $("#specialization_name").val(res.specialization_name);
            $("#modal_specialization").modal('show');
        });
    });

    // Delete Specialization
    $("body").on('click', '#delete_item', function() {
        var id = $(this).data('id');
        confirm('Are you sure want to delete !');

        $.ajax({
            type: 'DELETE',
            url: "specializations/destroy/" + id
        }).done(function(res) {
            $("#row_item_" + id).remove();
        });
    });

    //Save specialization            
    $("form").on('submit', function(e) {
        e.preventDefault();        
        $.ajax({
            url: "specializations/store",
            data: $("#form_specialization").serialize(),
            type: 'POST'
        }).done(function(res){            
                var row = '<tr id="row_item_' + res.id + '">';
                row += '<td width="20">' + res.id + '</td>';
                row += '<td>' + res.specialization_name + '</td>';
                row += '<td>' + res.specialization_slug + '</td>';
                row += '<td width="150">' + '<button type="button" id="edit_item" data-id="' + res.id + '" class="btn btn-info btn-sm mr-1">Edit</button>' + '<button type="button" id="delete_item" data-id="' + res.id + '" class="btn btn-danger btn-sm">Delete</button>' + '</td>';

                if ($("#id").val()) {
                    $("#row_item_" + res.id).replaceWith(row);
                    location.reload();
                } else {
                    $("#list_specializations").prepend(row);
                    location.reload();
                }

                $("#form_specialization").trigger('reset');
                $("#modal_specialization").modal('hide');
                
                $('#success_div').removeClass('d-none');
                $('#success_msg').html(res.msg);

        });
    });
    
</script>
@endsection