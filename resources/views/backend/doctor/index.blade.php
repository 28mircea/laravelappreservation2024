@extends('admin.admin_dashboard')
@section('admin')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.5.0/font/bootstrap-icons.min.css' />

<div class="container" style="width: 950px;">
    <div class="row my-5">
        <div class="col-lg-12 margin-tb">
            <div class="card shadow">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h3 class="text-dark">Manage Doctors</h3>
                    <button class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#addDoctorModal"><i class="bi-plus-circle me-2"></i>Add New Doctor</button>
                </div>
                <div class="card-body" id="show_all_doctors">
                    <h1 class="text-center text-secondary my-5">Loading...</h1>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- new doctor modal --}}
<div class="modal fade" id="addDoctorModal" tabindex="-1" aria-labelledby="exampleModalLabel" data-bs-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New Doctor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>
            <form action="#" id="add_doctor_form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body p-4 bg-light">
                    <div class="row">
                        <div class="col-lg">
                            <label for="full_name">Full Name</label>
                            <input type="text" name="full_name" class="form-control" placeholder="Full Name" required>
                        </div>
                        <div class="my-2">
                            <label for="image">Select Image</label>
                            <input type="file" name="image" class="form-control" required>
                        </div>
                        <div class="col-lg">
                            <label for="professional_statement">Professional statement</label>
                            <input type="text" name="professional_statement" class="form-control" placeholder="Professional statement" required>
                        </div>
                    </div>
                    <div class="my-2">
                        <label for="app_price">Appointment Price</label>
                        <input type="number" name="app_price" class="form-control" placeholder="Appointment Price" required>
                    </div>
                    <div class="form-group">
                        <label for="specializations">Select Specialization</label>
                        <div class="mt-1">
                            <select id="specializations" name="specializations" class="form-control">
                                <option value="" selected="selected">(select specialization)</option>
                                @foreach ($specializations as $specialization)
                                <option value="{{ $specialization->id }}">
                                    {{ $specialization->specialization_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning" id="add_doctor_btn" class="btn btn-primary">Add Doctor</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end bootstrap modal -->
{{-- edit doctor modal --}}
<div class="modal fade" id="editDoctorModal" tabindex="-1" aria-labelledby="exampleModalLabel" data-bs-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Doctor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="#" id="edit_doctor_form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="doc_id" id="doc_id">
                <input type="hidden" name="doc_image" id="doc_image">
                <div class="modal-body p-4 bg-light">
                    <div class="row">
                        <div class="col-lg">
                            <label for="full_name">Full Name</label>
                            <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Full Name" required>
                        </div>
                        <div class="my-2">
                            <label for="image">Select Image</label>
                            <input type="file" name="image" id="image" class="form-control">
                        </div>
                        <div class="mt-2" id="image-old"></div>
                        <div class="col-lg">
                            <label for="professional_statement">Professional statement</label>
                            <input type="text" name="professional_statement" id="professional_statement" class="form-control" placeholder="Professional statement" required>
                        </div>
                    </div>
                    <div class="my-2">
                        <label for="app_price">Appointment Price</label>
                        <input type="number" name="app_price" id="app_price" class="form-control" placeholder="Appointment Price" required>
                    </div>
                    <div class="form-group">
                        <label for="specializations">Select Specialization</label>
                        <div class="mt-1">
                            <select id="specializations" name="specializations" class="form-control">
                                @foreach ($specializations as $specialization)
                                <option value="{{$specialization->specialization_id}}" ${`{{$specialization->id}}`==i.specialization_id ? 'selected' : '' }>{{$specialization->specialization_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" id="edit_doctor_btn" class="btn btn-success">Update Doctor</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end edit doctor modal --}}
<script type="text/javascript">
    $(function() {

        // add new doctor ajax request
        $("#add_doctor_form").submit(function(e) {
            e.preventDefault();
            const fd = new FormData(this);
            $("#add_doctor_btn").text('Adding...');
            $.ajax({
                url: "{{ route('doctors.store') }}",
                method: 'post',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response) {
                    if (response.status == 200) {
                        Swal.fire(
                            'Added!',
                            'Doctor Added Successfully!',
                            'success'
                        )
                        fetchAllDoctors();
                    }
                    $("#add_doctor_btn").text('Add Doctor');
                    $("#add_doctor_form")[0].reset();                    
                    $("#addDoctorModal").modal('hide');
                    window.location.reload();
                }
            });
        });

        // edit doctor ajax request
        $(document).on('click', '.editIcon', function(e) {
            e.preventDefault();
            let id = $(this).attr('id');
            $.ajax({
                url: "{{ route('doctors.edit') }}",
                method: 'get',
                data: {
                    id: id,
                    _token: '{{ csrf_token() }}'
                },
                success: function(response) {
                    $("#full_name").val(response.full_name);
                    $("#professional_statement").val(response.professional_statement);
                    $("#app_price").val(response.app_price);
                    $("#specializations").val(response.specializations);
                    $("#image-old").html(
                        `<img src="storage/doctors/${response.image}" width="100" class="img-fluid img-thumbnail">`);
                    $("#doc_id").val(response.id);
                    $("#doc_image").val(response.image);
                }
            });
        });

        //update doctor ajax request
        $("#edit_doctor_form").submit(function(e) {
            e.preventDefault();
            const fd = new FormData(this);
            $("#edit_doctor_btn").text('Updating...');
            $.ajax({
                url: "{{ route('doctors.update') }}",
                method: 'post',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response) {
                    if (response.status == 200) {
                        Swal.fire(
                            'Updated!',
                            'Doctor Updated Successfully!',
                            'success'
                        )
                        fetchAllDoctors();
                        window.location.reload();
                    }
                    $("#edit_doctor_btn").text('Update Doctor');
                    $("#edit_doctor_form")[0].reset();
                    $("#editDoctorModal").modal('hide');

                }
            });
        });

        // delete doctor ajax request
        $(document).on('click', '.deleteIcon', function(e) {
            e.preventDefault();
            let id = $(this).attr('id');
            let csrf = '{{ csrf_token() }}';
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{ route('doctors.delete') }}",
                        method: 'delete',
                        data: {
                            id: id,
                            _token: csrf
                        },
                        success: function(response) {
                            console.log(response);
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                            fetchAllDoctors();
                        }
                    });
                }
            })
        });

        // fetch all doctors ajax request
        fetchAllDoctors();

        function fetchAllDoctors() {
            $.ajax({
                url: "{{ route('doctors.fetchall') }}",
                method: 'get',
                success: function(response) {
                    $("#show_all_doctors").html(response);
                    $("table").DataTable({
                        order: [0, 'asc']
                    });
                }
            });
        }
    });

    
</script>
@endsection