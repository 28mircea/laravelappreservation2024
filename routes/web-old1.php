<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\SpecializationController;
use App\Http\Controllers\Admin\DoctorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::middleware(['auth','admin'])->name('admin.')->group(function() {
    Route::get('/admin', [AdminController::class, 'AdminDashboard'])->name('dashboard');
    Route::get('/admin/logout', [AdminController::class, 'AdminLogout'])->name('logout');
    Route::get('/admin/profile', [AdminController::class, 'AdminProfile'])->name('profile');
    Route::post('/admin/profile/store', [AdminController::class, 'AdminProfileStore'])->name('profile.store');
    Route::get('/admin/change/password', [AdminController::class, 'AdminChangePassword'])->name('change.password');
    Route::post('/admin/password/update', [AdminController::class, 'AdminPasswordUpdate'])->name('password.update');
    
});
    
Route::middleware(['auth','admin'])->group(function() {

    //routes for specialization
    Route::get('/specializations', [SpecializationController::class,'showSpecializations'])->name('showSpecializations');
    Route::post('specializations/store', [SpecializationController::class,'addSpecialization']);
    Route::get('specializations/{specialization}/edit', [SpecializationController::class,'editSpecialization']);
    Route::put('specializations/{specialization}/update', [SpecializationController::class,'updateSpecialization']);
    Route::delete('specializations/destroy/{specialization}', [SpecializationController::class,'deleteSpecialization']);
    
    //routes for doctor
    Route::get('/doctors', [DoctorController::class,'showDoctors'])->name('showDoctors');
    Route::post('doctors/store', [DoctorController::class,'addDoctor']);
    Route::get('doctors/{id}/edit', [DoctorController::class,'editDoctor']);
    Route::post('doctors/{doctor}/update', [DoctorController::class,'updateDoctor']);
    Route::delete('doctors/destroy/{doctor}', [DoctorController::class,'deleteDoctor']);

    
});



require __DIR__.'/auth.php';
