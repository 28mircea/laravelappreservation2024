<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\SpecializationController;
use App\Http\Controllers\Admin\DoctorController;
use App\Http\Controllers\Admin\AppointmentController;
use App\Http\Controllers\Frontend\SpecializationController as FrontendSpecializationController;
use App\Http\Controllers\Frontend\DoctorController as FrontendDoctorController;
use App\Http\Controllers\Frontend\AppointmentController as FrontendAppointmentController;
use App\Http\Controllers\Frontend\WelcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('front-specializations', [FrontendSpecializationController::class, 'index'])->name('front.specializations.index');
Route::get('front-specializations/{specialization}', [FrontEndSpecializationController::class, 'show'])->name('front.specializations.show');
Route::get('front-doctors', [FrontendDoctorController::class, 'index'])->name('front.doctors.index');
Route::get('/appointments/appForm/step-one', [FrontEndAppointmentController::class, 'appFormStepOne'])->name('appointments.appForm.step.one');
Route::post('/appointments/appForm/step-one',[FrontEndAppointmentController::class,'storeAppFormStepOne'])->name('appointments.storeAppForm.step.one');
Route::get('/appointments/appForm/step-two', [FrontEndAppointmentController::class, 'appFormStepTwo'])->name('appointments.appForm.step.two');
Route::post('/appointments/appForm/step-two',[FrontEndAppointmentController::class,'storeAppFormStepTwo'])->name('appointments.storeAppForm.step.two');
Route::get('/thankyou', [WelcomeController::class, 'thankyou'])->name('thankyou');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::middleware(['auth','admin'])->name('admin.')->group(function() {
    Route::get('/admin', [AdminController::class, 'AdminDashboard'])->name('dashboard');
    Route::get('/admin/logout', [AdminController::class, 'AdminLogout'])->name('logout');
    Route::get('/admin/profile', [AdminController::class, 'AdminProfile'])->name('profile');
    Route::post('/admin/profile/store', [AdminController::class, 'AdminProfileStore'])->name('profile.store');
    Route::get('/admin/change/password', [AdminController::class, 'AdminChangePassword'])->name('change.password');
    Route::post('/admin/password/update', [AdminController::class, 'AdminPasswordUpdate'])->name('password.update');
    
});
    
Route::middleware(['auth','admin'])->group(function() {

    //routes for specialization
    Route::get('/specializations', [SpecializationController::class,'showSpecializations'])->name('showSpecializations');
    Route::post('specializations/store', [SpecializationController::class,'addSpecialization']);
    Route::get('specializations/{specialization}/edit', [SpecializationController::class,'editSpecialization']);
    Route::put('specializations/{specialization}/update', [SpecializationController::class,'updateSpecialization']);
    Route::delete('specializations/destroy/{specialization}', [SpecializationController::class,'deleteSpecialization']);
    
    //routes for doctor
    Route::get('/doctors',  [DoctorController::class,'index'])->name('doctors.index');
    Route::post('/store', [DoctorController::class, 'store'])->name('doctors.store');
    Route::get('/fetchall', [DoctorController::class, 'fetchAll'])->name('doctors.fetchall');
    Route::delete('/destroy', [DoctorController::class, 'destroy'])->name('doctors.delete');
    Route::get('/edit', [DoctorController::class, 'edit'])->name('doctors.edit');
    Route::post('/update', [DoctorController::class, 'update'])->name('doctors.update'); 
    
    //routes for appointments
    Route::get('/appointments', [AppointmentController::class, 'index'])->name('appointments.index');
    Route::get('/appointments/create', [AppointmentController::class, 'create'])->name('appointments.create');
    Route::post('/appointments/store', [AppointmentController::class, 'store'])->name('appointments.store');
    Route::get('/appointments/edit/{appointment}', [AppointmentController::class, 'edit'])->name('appointments.edit');
    Route::put('/appointments/update/{appointment}', [AppointmentController::class, 'update'])->name('appointments.update');
    Route::delete('/appointments/delete/{appointment}', [AppointmentController::class, 'destroy'])->name('appointments.delete');

});



require __DIR__.'/auth.php';
