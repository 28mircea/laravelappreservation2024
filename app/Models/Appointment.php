<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'app_time' => 'datetime:H:i'
    ];  

    public function doctor(){
        return $this->BelongsTo(Doctor::class);
    }

}
