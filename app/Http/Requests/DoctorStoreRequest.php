<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoctorStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'full_name' => ['required'],           
            'image' => ['required'],
            'professional_statement' => ['required'],
            'app_price' => ['required'],  
        ];
    }

    public function messages()
    {
        return [
            'full_name.required' => 'Full name is required!',
            'professinal_statement.required'  => 'Professional statement is required!',
            'app_price.required' => 'Appointment price is required!',
            'app_price.integer' => 'Price must be an integer value!',            
            'image' => 'File uploaded must be jpeg,jpg,bmp,png type!'
        ];
    }
}
