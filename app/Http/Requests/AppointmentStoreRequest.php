<?php

namespace App\Http\Requests;

use App\Models\Appointment;
use Illuminate\Foundation\Http\FormRequest;

class AppointmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function prepareForValidation(){
        $this->isValid();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'full_name' => ['required'],           
            'email' => ['required', 'email'],
            'tel_number' => ['required'],
            'app_date' => ['required','date_format:Y-m-d'],
            'app_time' => ['required','date_format:H:i'],
            'doctor_id' => ['required']
        ];
    }


    private function isValid(){        

        $isAlreadyExists = Appointment::where('doctor_id', $this->input('doctor_id'))->where('app_date', $this->input('app_date'))->where('app_time', $this->input('app_time'))->exists();

        if ($isAlreadyExists){
            abort(422, 'The appointment is already taken!');
        }
    }
}
