<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\DoctorStoreRequest;
use App\Models\Doctor;
use App\Models\Specialization;
use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Facades\Validator;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $specializations = Specialization::all(); 
        $doctors = Doctor::all();       
        return view('backend.doctor.index', compact(['specializations','doctors']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function fetchAll() {
        $doctors = Doctor::all();
        $output = '';
        if ($doctors->count() > 0) {
            $output .= '<table class="table table-striped align-middle">
            <thead>
              <tr>
                <th>Id</th>
                <th>Full Name</th>
                <th>Image</th>
                <th>Professional Statement</th>
                <th>Appointment Price</th>
                <th>SpecializationId</th>                                    
                <th>Action</th>
              </tr>
            </thead>
            <tbody>';
            foreach ($doctors as $doctor) {
                $output .= '<tr>
                <td>' . $doctor->id . '</td>
                <td>' . $doctor->full_name . '</td>
                <td><img src="/storage/doctors/' . $doctor->image . '" width="50" class="img-thumbnail rounded-circle"></td>
                <td>' . $doctor->professional_statement . '</td>
                <td>' . $doctor->app_price . '</td>
                <td>' . $doctor->specialization_id . '</td>                
                <td>
                  <a href="#" id="' . $doctor->id . '" class="text-success mx-1 editIcon" data-bs-toggle="modal" data-bs-target="#editDoctorModal"><i class="bi-pencil-square h4"></i></a>
                  <a href="#" id="' . $doctor->id . '" class="text-danger mx-1 deleteIcon"><i class="bi-trash h4"></i></a>
                </td>
              </tr>';
            }
            $output .= '</tbody></table>';
            echo $output;
        } else {
            echo '<h1 class="text-center text-secondary my-5">No record in the database!</h1>';
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
            $file = '';
            
            if($request->image->getClientOriginalName()){
                $ext = $request->image->getClientOriginalExtension();
                $file = date('YmdHis').rand(1,99999).'.'.$ext;
                $request->image->storeAs('public/doctors', $file);
            }       
                
            Doctor::create([
                'full_name' => $request->full_name,           
                'image' => $file,
                'professional_statement' => $request->professional_statement,
                'app_price' => $request->app_price,
                'specialization_id' => $request->specializations,           
            ]);

            return response()->json([ 'status' => 200,]);
                  
    }

    
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request)
    {
        $id = $request->id;
        $doc = Doctor::find($id);
        return response()->json($doc);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $fileName = '';
        $doc = Doctor::find($request->doc_id);       
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/doctors', $fileName);
            if ($doc->image) {
                Storage::delete('public/doctors/' . $doc->image);
            }
        } else {
            $fileName = $request->doc_image;
        }

        $doc->update([
            'full_name' => $request->full_name,            
            'image' => $fileName,
            'professional_statement' => $request->professional_statement,
            'app_price' => $request->app_price,
            'specialization_id' => $request->specializations,           
        ]);

        return response()->json([ 'status' => 200,]); 
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $doctor = Doctor::find($id);
        if (Storage::delete('public/doctors/' . $doctor->image)) {
            Doctor::destroy($id);
        }
    }
    
}
