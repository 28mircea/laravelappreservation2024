<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{
    
    public function AdminDashboard(){
        return view('admin.index');
    }


    public function AdminProfile(){

        $id = Auth::user()->id;
        $profileData = User::find($id);
  
        return view('admin.admin_profile_view',compact('profileData'));
  
    }


    public function AdminProfileStore(Request $request){

        $id = Auth::user()->id;
        $data = User::find($id);
  
        $data->name = $request->name;
        $data->email = $request->email;   
        
        $data->save();
  
        $notification = array(
           'message' => 'Admin Profile Updated Successfully',
           'alert-type' => 'success',
        );
  
        return redirect()->back()->with($notification);  
    }


    public function AdminChangePassword(){

        $id = Auth::user()->id;
        $profileData = User::find($id);
  
        return view('admin.admin_change_password',compact('profileData'));
  
    }


    public function AdminPasswordUpdate(Request $request){

        //validation
        $request -> validate([
           'old_password' => 'required',
           'new_password' => 'required|confirmed'
  
        ]);  
  
        if(!Hash::check($request->old_password, auth::user()->password)){
  
           $notification = array(
              'message' => 'Old Password Does Not Match',
              'alert-type' => 'error',
           );
     
           return back()->with($notification);
  
        }  
  
        //update the new password
        User::whereId(auth::user()->id)->update([
           'password' => Hash::make($request->new_password)
        ]);
  
        $notification = array(
           'message' => 'Password Updated Successfully',
           'alert-type' => 'success',
        );
  
        return back()->with($notification);
  
    }



    public function AdminLogout(Request $request)
    {
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }

}
