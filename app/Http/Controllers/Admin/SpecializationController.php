<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Specialization;
use Illuminate\Support\Facades\Validator;


class SpecializationController extends Controller
{
    
    public function showSpecializations(){
      
        $specializations = Specialization::all();        
        return view('backend.specialization.index', compact('specializations'));
    }



    public function addSpecialization(Request $request){

        $validator = Validator::make($request->all(), [
            'specialization_name' => 'required',
        ],
        [
            'specialization_name.required' => 'Please enter specialization name',    
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'error' => $validator->errors()->all()
                    ]);
        }        
        else{
            $specialization = Specialization::updateOrCreate(          
                ['id' => $request->id],
                ['specialization_name' => $request->specialization_name, 
                'specialization_slug' => strtolower(str_replace(' ','-', $request->specialization_name))
                ]);
        }

        $arr = array($specialization, 'msg' => 'Successfully Specialization Submit', 'status' => true);
        return response()->json($arr);
    }

    

   public function editSpecialization(Specialization $specialization){
       
        return response()->json($specialization);
    }

    public function deleteSpecialization(Specialization $specialization){
        $specialization->delete();
        return response()->json('success');
    }

}
