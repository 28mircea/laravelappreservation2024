<?php

namespace App\Http\Controllers\Admin;

use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\Specialization;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Carbon\CarbonInterval;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentStoreRequest;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $appointments = Appointment::all();
        return view('backend.appointment.index', compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $doctors = Doctor::all();
        $specializations = Specialization::all();
        return view('backend.appointment.create', compact(['doctors','specializations']));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AppointmentStoreRequest $request){
        
        Appointment::create(request(['full_name','email','tel_number','app_date','app_time','doctor_id']));
       
        return redirect()->route('appointments.index')->with('success', 'Appointment created successfully!');

    }

    /**
     * Display the specified resource.
     */
    //public function show(string $id)
    //{
        //
    //}

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Appointment $appointment){

        $doctors = Doctor::all();
        return view('backend.appointment.edit', compact('appointment','doctors'));
    }


    public function update(AppointmentStoreRequest $request, Appointment $appointment){        
        
        $appointment->update($request->all());
        return redirect()->route('appointments.index')->with('success', 'Appointment updated successfully!');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Appointment $appointment){
        
        $appointment->delete();
        return redirect()->route('appointments.index')->with('success', 'Appointment deleted successfully!');

    }
}
