<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctor;
use App\Models\Specialization;

class SpecializationController extends Controller
{
    public function index(){
        $specializations = Specialization::all();
        return view('specializations.index', compact('specializations'));
    }
    
    public function show(Specialization $specialization){
        //$doctors = Doctor::where('status', DoctorStatus::Available)->get();
        return view('specializations.show', compact('specialization'));
    }
}
