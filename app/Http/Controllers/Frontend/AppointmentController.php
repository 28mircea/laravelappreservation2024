<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Appointment;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function appFormStepOne(Request $request){

        $appointment = $request->session()->get('appointment');
        return view('appointments.appForm-step-one', compact('appointment'));

    }


    public function storeAppFormStepOne(Request $request){

        $validated = $request->validate([
            'full_name' => ['required'],           
            'email' => ['required', 'email'],            
            'tel_number' => ['required'],            
            'app_date' => ['required'],
            'app_time' => ['required']
        ]);

        if (empty($request->session()->get('appointment'))) {
            $appointment = new Appointment();
            $appointment->fill($validated);
            $request->session()->put('appointment', $appointment);
                      

        } else {
            $appointment = $request->session()->get('appointment');
            $appointment->fill($validated);
            $request->session()->put('appointment', $appointment);
        }
               
        return to_route('appointments.appForm.step.two');        

    }


    public function appFormStepTwo(Request $request){

        $appointment = $request->session()->get('appointment'); 
       
        $app_doctor_ids = Appointment::orderBy('app_time')->get()->filter(function($value) use($appointment){
            return $value->app_time->format('H:i') == $appointment->app_time->format('H:i'); 
        })->pluck('doctor_id');        

        $doctors = Doctor::whereNotIn('id', $app_doctor_ids)->get();

        return view('appointments.appForm-step-two', compact('appointment','doctors'));

    }


    public function storeAppFormStepTwo(Request $request){

        $validated = $request->validate([            
            'doctor_id'=>  ['required']            
        ]);

        $appointment = $request->session()->get('appointment');
        $appointment->fill($validated);
        $appointment->save();
        $request->session()->forget('appointment');

        return to_route('thankyou');        

    }

}
